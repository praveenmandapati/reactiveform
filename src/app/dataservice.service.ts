import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from 'rxjs';
const baseUrl = 'https://jsonplaceholder.typicode.com';
@Injectable({
  providedIn: 'root'
})
export class DataserviceService {

  constructor(private httpClient: HttpClient) { }

  create(data:any) {
    return this.httpClient.post(baseUrl + '/posts/', data);
  }
  getdata():Observable<any>{
    return this.httpClient.get<any>(baseUrl + '/todos/');
  }

  getcomments():Observable<any>{
    return this.httpClient.get<any>(baseUrl + '/comments/');
  }
}
