import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataserviceService } from '../dataservice.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  values: any;
  comments: any;

  constructor(private router: Router,
    private dataservice: DataserviceService,) { }

  ngOnInit(): void {

    this.dataservice.getdata().subscribe(data=>{
     
      const size = 6
      this.values = data.slice(0, size)
      //console.log(this.values)
    })

    this.dataservice.getcomments().subscribe(data=>{
      const size = 2
      this.comments = data.slice(0, size)
      console.log(this.comments)
    })
  }
  navigateToRoute(path: string) {
    this.router.navigate([path]);
  }

  navigateTo(path: string) {
    this.router.navigate([path]);
  }

  navigate(path: string) {
    this.router.navigate([path]);
  }
  navigateback(){
    this.router.navigate(['/login']);
  }
}
